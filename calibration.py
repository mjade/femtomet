from graphics import *
import numpy as np


######################################################################################################################
infile1 = 'data/Botev1-calib.TXT'  # Data from Botev 1
infile2 = 'data/Botev2-calib.TXT'  # Data from Botev 2

# Plot labels
title1 = "Botev 1"
title2 = "Botev 2"

# Calibration coefficients
botev1_temp_coeff = 0.  # Temperature sensor calibration for Botev 1
botev2_temp_coeff = 0.  # Temperature sensor calibration for Botev 2
botev1_pres_coeff = 0.  # Pressure sensor calibration for Botev 1
botev2_pres_coeff = 0.  # Pressure sensor calibration for Botev 2
botev1_humd_coeff = 0.  # Humidity sensor calibration for Botev 1
botev2_humd_coeff = 0.  # Humidity sensor calibration for Botev 2

# Limits as datetime objects (optional). If undefined, the recorded time series limits will be used.
limits = True  # Set to True for user-defined limits. If False, the recorded time series limits will be used.1
start = datetime.datetime(year=2020, month=7, day=23, hour=22, minute=24)
end = datetime.datetime(year=2020, month=7, day=24, hour=8, minute=45)
# Note: Choose an interval with a linear difference.
######################################################################################################################


def main():
    time_1, temp_1, pres_1, humd_1 = [], [], [], []
    time_2, temp_2, pres_2, humd_2 = [], [], [], []

    # Read Botev 1
    i_all, i_true = 0, 0
    file1 = open(infile1, 'r')
    for line in file1:
        i_all += 1
        if check_line(line) is True:
            i_true += 1
            record_values = read_line(line)
            if limits is True:
                time_test = record_values[0]
                if not start <= time_test <= end:
                    continue
            time_1.append(record_values[0])
            temp_1.append(record_values[1])
            pres_1.append(record_values[2])
            humd_1.append(record_values[3])
    print('Botev 1 has %i valid and %i invalid records.' % (i_true, i_all-i_true))

    # Read Botev 2
    i_all, i_true = 0, 0
    file2 = open(infile2, 'r')
    for line in file2:
        i_all += 1
        if check_line(line) is True:
            i_true += 1
            record_values = read_line(line)
            if limits is True:
                time_test = record_values[0]
                if not start <= time_test <= end:
                    continue
            time_2.append(record_values[0])
            temp_2.append(record_values[1])
            pres_2.append(record_values[2])
            humd_2.append(record_values[3])
    print('Botev 2 has %i valid and %i invalid records.' % (i_true, i_all-i_true))

    # Smooth data in 5 min intervals
    time_1, temp_1, pres_1, humd_1 = smoothing_average(time_1, temp_1, pres_1, humd_1)
    time_2, temp_2, pres_2, humd_2 = smoothing_average(time_2, temp_2, pres_2, humd_2)

    # Apply calibration coefficients:
    temp_1 = [val + botev1_temp_coeff for val in temp_1]
    temp_2 = [val + botev2_temp_coeff for val in temp_2]
    pres_1 = [val + botev1_pres_coeff for val in pres_1]
    pres_2 = [val + botev2_pres_coeff for val in pres_2]
    humd_1 = [val + botev1_humd_coeff for val in humd_1]
    humd_2 = [val + botev2_humd_coeff for val in humd_2]

    fig, ax = plt.subplots()
    if limits is True:  ax.set_xlim(start, end)
    ax.set_xlabel("Time, UTC")
    ax.set_ylabel("Temperature [°C]")
    plot1 = ax.plot(time_1, temp_1, linewidth=0.6, color='red', label=title1)
    plot2 = ax.plot(time_2, temp_2, linewidth=0.6, color='blue', label=title2)

    myFmt = mdates.DateFormatter('%Y-%m-%d %H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    plt.gcf().autofmt_xdate()
    ax.legend(loc='best')

    fig.tight_layout()
    # plt.show()
    plt.savefig('botev_temp_calibr.png', format='png', dpi=500)
    plt.close('all')

    fig, ax = plt.subplots()
    if limits is True:  ax.set_xlim(start, end)
    ax.set_xlabel("Time, UTC")
    ax.set_ylabel("Humidity [%]")
    plot1 = ax.plot(time_1, humd_1, linewidth=0.6, color='red', label=title1)
    plot2 = ax.plot(time_2, humd_2, linewidth=0.6, color='blue', label=title2)

    myFmt = mdates.DateFormatter('%Y-%m-%d %H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    plt.gcf().autofmt_xdate()
    ax.legend(loc='best')

    fig.tight_layout()
    plt.savefig('botev_humd_calibr.png', format='png', dpi=500)
    plt.close('all')

    fig, ax = plt.subplots()
    if limits is True:  ax.set_xlim(start, end)
    ax.set_xlabel("Time, UTC")
    ax.set_ylabel("Pressure [hPa]")
    plot1 = ax.plot(time_1, pres_1, linewidth=0.6, color='red', label=title1)
    plot2 = ax.plot(time_2, pres_2, linewidth=0.6, color='blue', label=title2)

    myFmt = mdates.DateFormatter('%Y-%m-%d %H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    plt.gcf().autofmt_xdate()
    ax.legend(loc='best')

    fig.tight_layout()
    plt.savefig('botev_pres_calibr.png', format='png', dpi=500)
    plt.close('all')

    temp_diff = np.nanmean(temp_1) - np.nanmean(temp_2)
    pres_diff = np.nanmean(pres_1) - np.nanmean(pres_2)
    humd_diff = np.nanmean(humd_1) - np.nanmean(humd_2)

    print("The average temperature difference is: ", temp_diff)
    print("The average pressure difference is: ", pres_diff)
    print("The average humidity difference is: ", humd_diff)

    adj_temp_1 = temp_diff/2
    adj_temp_2 = temp_diff/2 * -1
    adj_pres_1 = pres_diff/2
    adj_pres_2 = pres_diff/2 * -1
    adj_humd_1 = humd_diff/2
    adj_humd_2 = humd_diff/2 * -1

    print("Suggested adjustment values")
    print("For Botev 1: temperature, pressure, humidity")
    print(adj_temp_1, adj_pres_1, adj_humd_1)
    print("For Botev 2: temperature, pressure, humidity")
    print(adj_temp_2, adj_pres_2, adj_humd_2)


if __name__ == "__main__":
    main()

import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from numpy import nanmean
from numpy import nan

######################################################################################################################
infile1 = 'data/Botev1-Botev.TXT'  # Data from Botev 1
infile2 = 'data/Botev2-Botev.TXT'  # Data from Botev 2

# Plot labels
title1 = "Snow spot"
title2 = "Botev peak"

# Calibration coefficients
botev1_temp_coeff = -0.29  # Temperature sensor calibration for Botev 1
botev2_temp_coeff = 0.29  # Temperature sensor calibration for Botev 2
botev1_pres_coeff = -0.1218  # Pressure sensor calibration for Botev 1
botev2_pres_coeff = 0.1218  # Pressure sensor calibration for Botev 2
botev1_humd_coeff = 0.78  # Humidity sensor calibration for Botev 1
botev2_humd_coeff = -0.78  # Humidity sensor calibration for Botev 2
# Note: These coefficients should be coherent with the measurement accuracy. The temperature accuracy is 0.01 degC,
# the pressure - 0.01 Pa (or 0.0001 hPa), the humidity - 0.01 % => the calibration coefficients shouldn't be
# lower than these values.

# Limits as datetime objects (optional). If undefined, the recorded time series limits will be used.
limits = False  # Set to True for user-defined limits. If False, the recorded time series limits will be used.1
start = datetime.datetime(year=2020, month=8, day=1, hour=12)  # Time series start. (Won't be used if limits is False.)
end = datetime.datetime(year=2020, month=8, day=1, hour=12)  # Time series end. (Won't be used if limits is False.)
######################################################################################################################


def check_line(l):
   if l[0:3] == '===':  # checks for new measurement
       return False
   date_check = l.split('/')
   if int(date_check[2][0:4]) == 2000 or int(date_check[2][0:4]) > 2050:  # checks for an invalid year
       return False
   try:
       date_record = datetime.datetime.strptime(l.split(',')[0], '%d/%m/%Y %H:%M:%S')
   except:
       return False
   if not 0 < int(date_check[0]) <= 31:  # checks for an invalid day
       return False
   return True


def read_line(l):
    date_record = datetime.datetime.strptime(l.split(',')[0], '%d/%m/%Y %H:%M:%S')
    record = l.replace(',', '').split('\t')
    temperature = float(record[1])
    pressure = float(record[2])/100.
    humidity = float(record[3])
    return [date_record, temperature, pressure, humidity]


def dates_average(dates):
    ref_date = datetime.datetime(1900, 1, 1)
    return ref_date + sum([date - ref_date for date in dates], datetime.timedelta()) / len(dates)


def smoothing_average(t, y1, y2, y3):
    t_new, y1_new, y2_new, y3_new = [], [], [], []
    j_last = 0
    for j in range(1, len(t) - 1):
        # if not t[j - 1] <= t[j] <= t[j + 1]:
        #     continue
        delta_x = t[j] - t[j_last]
        if datetime.timedelta(minutes=5) <= delta_x <= datetime.timedelta(minutes=45):
            t_mean = dates_average(t[j_last:j])
            y1_mean = nanmean(y1[j_last:j])
            y2_mean = nanmean(y2[j_last:j])
            y3_mean = nanmean(y3[j_last:j])
            t_new.append(t_mean)
            y1_new.append(y1_mean)
            y2_new.append(y2_mean)
            y3_new.append(y3_mean)
            j_last = j
        if delta_x > datetime.timedelta(minutes=45):
            missed_count = int(delta_x/datetime.timedelta(minutes=5))
            t_ext = [t[j_last+missed]+datetime.timedelta(minutes=5) for missed in range(0, missed_count)]
            y1_mean = [nan] * missed_count
            y2_mean = [nan] * missed_count
            y3_mean = [nan] * missed_count
            t_new.extend(t_ext)
            y1_new.extend(y1_mean)
            y2_new.extend(y2_mean)
            y3_new.extend(y3_mean)
            j_last = j
    return t_new, y1_new, y2_new, y3_new


def main():
    time_1, temp_1, pres_1, humd_1 = [], [], [], []
    time_2, temp_2, pres_2, humd_2 = [], [], [], []

    # Read Botev 1
    i_all, i_true = 0, 0
    file1 = open(infile1, 'r')
    for line in file1:
        i_all += 1
        if check_line(line) is True:
            i_true += 1
            record_values = read_line(line)
            time_1.append(record_values[0])
            temp_1.append(record_values[1])
            pres_1.append(record_values[2])
            humd_1.append(record_values[3])
    print('Botev 1 has %i valid and %i invalid records.' % (i_true, i_all-i_true))

    # Read Botev 2
    i_all, i_true = 0, 0
    file2 = open(infile2, 'r')
    for line in file2:
        i_all += 1
        if check_line(line) is True:
            i_true += 1
            record_values = read_line(line)
            time_2.append(record_values[0])
            temp_2.append(record_values[1])
            pres_2.append(record_values[2])
            humd_2.append(record_values[3])
    print('Botev 2 has %i valid and %i invalid records.' % (i_true, i_all-i_true))

    # Smooth data in 5 min intervals
    time_1, temp_1, pres_1, humd_1 = smoothing_average(time_1, temp_1, pres_1, humd_1)
    time_2, temp_2, pres_2, humd_2 = smoothing_average(time_2, temp_2, pres_2, humd_2)

    # Apply calibration coefficients:
    temp_1 = [val + botev1_temp_coeff for val in temp_1]
    temp_2 = [val + botev2_temp_coeff for val in temp_2]
    pres_1 = [val + botev1_pres_coeff for val in pres_1]
    pres_2 = [val + botev2_pres_coeff for val in pres_2]
    humd_1 = [val + botev1_humd_coeff for val in humd_1]
    humd_2 = [val + botev2_humd_coeff for val in humd_2]

    fig, ax = plt.subplots()
    if limits is True:  ax.set_xlim(start, end)
    ax.set_xlabel("Time, UTC")
    ax.set_ylabel("Temperature [°C]")
    plot1 = ax.plot(time_1, temp_1, linewidth=0.6, color='red', label=title1)
    plot2 = ax.plot(time_2, temp_2, linewidth=0.6, color='blue', label=title2)

    myFmt = mdates.DateFormatter('%Y-%m-%d %H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    plt.gcf().autofmt_xdate()
    ax.legend(loc='best')

    fig.tight_layout()
    plt.savefig('botev_temp.png', format='png', dpi=500)
    plt.close('all')

    fig, ax = plt.subplots()
    if limits is True:  ax.set_xlim(start, end)
    ax.set_xlabel("Time, UTC")
    ax.set_ylabel("Humidity [%]")
    plot1 = ax.plot(time_1, humd_1, linewidth=0.6, color='red', label=title1)
    plot2 = ax.plot(time_2, humd_2, linewidth=0.6, color='blue', label=title2)

    myFmt = mdates.DateFormatter('%Y-%m-%d %H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    plt.gcf().autofmt_xdate()
    ax.legend(loc='best')

    fig.tight_layout()
    plt.savefig('botev_humd.png', format='png', dpi=500)
    plt.close('all')

    fig, ax = plt.subplots()
    if limits is True:  ax.set_xlim(start, end)
    ax.set_xlabel("Time, UTC")
    ax.set_ylabel("Pressure [hPa]")
    plot1 = ax.plot(time_1, pres_1, linewidth=0.6, color='red', label=title1)
    plot2 = ax.plot(time_2, pres_2, linewidth=0.6, color='blue', label=title2)

    myFmt = mdates.DateFormatter('%Y-%m-%d %H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    plt.gcf().autofmt_xdate()
    ax.legend(loc='best')

    fig.tight_layout()
    plt.savefig('botev_pres.png', format='png', dpi=500)
    plt.close('all')


if __name__ == "__main__":
    main()
